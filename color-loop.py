import random
from colored import fg, bg, attr
reset = attr('reset')
while True:
    # Generates the hex color codes for foreground and backgroung colors
    random_number = random.randint(1118481, 16777215)
    hex_number = format(random_number, 'x')
    hex_number = '#' + hex_number
    random_number2 = random.randint(1118481, 16777215)
    hex_number2 = format(random_number2, 'x')
    hex_number2 = '#' + hex_number2
    color = fg(hex_number) + bg(hex_number2)
    # I reset because I don't want the entire line to be a color
    print(color + "COLOR", reset)